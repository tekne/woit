# Quantum Theory, Groups and Representations Notes

These are extremely compressed notes for Woit's *Quantum Theory, Groups and Representations*, produced mainly as a study aid, along with solutions to some of the exercises. These may, nay will, be incorrect/incomplete, so use at your own risk!